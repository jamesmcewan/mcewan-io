---
title: "Home"
date: 2018-12-21T01:14:22Z
menu:
  main:
    weight: -1
draft: false
---

# Hi Folks!

I'm James, a web developer based in Edinburgh, Scotland.

I've been building things for the web since around 1998. At the moment I mostly work with JavaScript to craft some pretty fun web apps.
